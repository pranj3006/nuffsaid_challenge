#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jan 25 09:47:02 2020

@author: pranjal.gharat
"""

import csv
import itertools
    
def print_total_schools():
    with open('school_data.csv', newline='',encoding='mac_roman') as csvfile:
        reader = csv.DictReader(csvfile)
        print("Total Schools:",str(sum( 1 for row in reader)))    
            

def print_total_schools_by_state():
    with open('school_data.csv', newline='',encoding='mac_roman') as csvfile:
        reader = csv.DictReader(csvfile)
        print("Schools by State:")
        lmb = lambda d: d["LSTATE05"]
        for k, g in itertools.groupby(reader,lmb):    
            counts = (1 for data in g)
            print("{0}: {1}".format(k,sum(counts)))
        

def print_total_schools_by_metro_centric_locale():
    with open('school_data.csv', newline='',encoding='mac_roman') as csvfile:
        reader = csv.DictReader(csvfile)    
        print("Schools by Metro-centric locale:")
        lmb = lambda d: d["MLOCALE"]
        for k, g in itertools.groupby(reader,lmb):    
            counts = (1 for data in g)
            print("{0}: {1}".format(k,sum(counts)))

def print_city_with_most_schools():
    with open('school_data.csv', newline='',encoding='mac_roman') as csvfile:
        reader = csv.DictReader(csvfile)            
        prv_count=0
        city_name=""
        lmb = lambda d: d["LCITY05"]
        for k, g in itertools.groupby(reader,lmb):    
            counts = (1 for data in g)
            school_count=sum(counts)        
            if school_count>prv_count:
                prv_count=school_count
                city_name=k    
        print("City with most schools: {0} ({1} schools)".format(city_name,prv_count))
    

def print_unique_cities_with_at_least_one_school():
    with open('school_data.csv', newline='',encoding='mac_roman') as csvfile:
        reader = csv.DictReader(csvfile)                
        lmb = lambda d: d["LCITY05"]
        city_counter=0
        for k, g in itertools.groupby(reader,lmb):    
            counts = (1 for data in g)
            school_count=sum(counts)        
            if school_count>0:
                city_counter=city_counter+1    
        print("Unique cities with at least one school: {0}".format(city_counter))    
    
    
def print_counts():
    print_total_schools()
    print_total_schools_by_state()
    print_total_schools_by_metro_centric_locale()
    print_city_with_most_schools()
    print_unique_cities_with_at_least_one_school()
    

