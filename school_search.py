#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jan 25 12:21:11 2020

@author: pranjal.gharat
"""

import csv
import time
search_in_data=list()
with open('school_data.csv', newline='',encoding='mac_roman') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        search_in_data.append(''.join([row['SCHNAM05'],"\n"+row['LCITY05'],", ",row['LSTATE05']]))
search_in_data.sort(key=lambda x: len(x), reverse=True)





def search_schools(school_name):
    start=time.time()
    match_inds=list()
    score=0
    for pos, item in enumerate(search_in_data):        
        tmp_score=[1 for wd in school_name.split() if wd in item]        
        if sum(tmp_score)>score:
            score=sum(tmp_score)
            match_inds.append([pos,score])
        
    match_inds.sort(key=lambda x: x[1],reverse=True)    
    end = time.time()
    timeval=end - start
    print('Results for "{0}" (search took: {1:.2}s'.format(s,timeval))
    for ind in range(1,4):
        if ind<=len(match_inds):
            print(str(ind)+". "+search_in_data[match_inds[ind-1][0]])
        else:
            break
    

#search_schools('RIVERSIDE SCHOOL 44')


